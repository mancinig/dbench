package it.mancini.dbench.exceptions;

import it.mancini.dbench.models.commons.DMLOperation;

public class BenchmarkNotEnabledException extends DbenchException {

	private static final long serialVersionUID = 1L;

	public BenchmarkNotEnabledException(DMLOperation operation) {
		super(String.format("%s benchmark is not enabled!", operation));
	}

}
