package it.mancini.dbench.exceptions;

import it.mancini.dbench.models.commons.DMLOperation;

public class UnsupportedDMLOperationException extends DbenchException {

	private static final long serialVersionUID = 1L;

	public UnsupportedDMLOperationException(DMLOperation operation) {
		super(String.format("Operation %s is not supported!", operation));
	}

}
