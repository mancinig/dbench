package it.mancini.dbench.exceptions;

import it.mancini.dbench.models.commons.DMLOperation;

public class BenchmarkExecutionException extends DbenchException {

	private static final long serialVersionUID = 1L;

	public BenchmarkExecutionException(DMLOperation operation, String error) {
		super(String.format("Error running benchmark %s! Message: %s", operation, error));
	}

}
