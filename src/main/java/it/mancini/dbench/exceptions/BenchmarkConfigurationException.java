package it.mancini.dbench.exceptions;

import it.mancini.dbench.models.commons.DMLOperation;

public class BenchmarkConfigurationException extends DbenchException {

	private static final long serialVersionUID = 1L;

	public BenchmarkConfigurationException(DMLOperation operation, String error) {
		super(String.format("Error running benchmark %s! Message: %s", operation, error));
	}

}
