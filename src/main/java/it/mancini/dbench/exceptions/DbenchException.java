package it.mancini.dbench.exceptions;

public class DbenchException extends Exception {

	private static final long serialVersionUID = 1L;

	public DbenchException() {
		super();
	}

	public DbenchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DbenchException(String message, Throwable cause) {
		super(message, cause);
	}

	public DbenchException(String message) {
		super(message);
	}

	public DbenchException(Throwable cause) {
		super(cause);
	}

}
