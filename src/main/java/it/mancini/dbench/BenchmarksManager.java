package it.mancini.dbench;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.exceptions.UnsupportedDMLOperationException;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.services.benchmarks.Benchmark;
import it.mancini.dbench.services.benchmarks.BenchmarkFactory;
import it.mancini.dbench.services.benchmarks.BenchmarkRequestsFactory;

@Service
public class BenchmarksManager {

	private Logger logger = LoggerFactory.getLogger(BenchmarksManager.class.getName());

	@Autowired
	private BenchmarkFactory benchmarkFactory;

	@Autowired
	private BenchmarkRequestsFactory benchmarkRequestsFactory;

	public <T extends BenchmarkBaseRequest> BenchmarkResult<T> executeBenchmark(DMLOperation operation)
			throws DbenchException {

		// retrieving the provider for the requested operation
		Benchmark benchmark = benchmarkFactory.getBenchmarkProvider(operation);

		// building the request from the configuration file.
		// Support for other configuration methods could be added.
		BenchmarkBaseRequest request = benchmarkRequestsFactory.buildRequestFromConfig(operation);

		// checking whether the benchmark is enabled
		if (!benchmark.isEnabled(request)) {
			logger.warn("Operation {} is not enabled! Check your configuration file.", operation);
			throw new UnsupportedDMLOperationException(operation);
		}

		// running
		return benchmark.execute(request);

	}

}
