package it.mancini.dbench.config.resultwriters;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "result.writer.console")
public class BenchmarkResultConsoleWriterConfig extends BenchmarkResultWriterConfig {

}
