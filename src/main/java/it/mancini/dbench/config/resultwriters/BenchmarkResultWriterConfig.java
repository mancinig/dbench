package it.mancini.dbench.config.resultwriters;

public abstract class BenchmarkResultWriterConfig {
	private boolean enabled;
	private boolean writeExecutions;
	private float timeFactor;
	private String unit;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isWriteExecutions() {
		return writeExecutions;
	}

	public void setWriteExecutions(boolean writeExecutions) {
		this.writeExecutions = writeExecutions;
	}

	public float getTimeFactor() {
		return timeFactor;
	}

	public void setTimeFactor(float timeFactor) {
		this.timeFactor = timeFactor;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
