package it.mancini.dbench.config.benchmarks;

public class BenchmarkBaseConfigFile implements BenchmarkBaseConfig {

	private boolean enabled;
	private String query;
	private int numOfExecutions;
	private int queriesBeforeTest;

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public String getQuery() {
		return query;
	}

	@Override
	public int getNumOfExecutions() {
		return numOfExecutions;
	}

	@Override
	public int getQueriesBeforeTest() {
		return queriesBeforeTest;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public void setNumOfExecutions(int numOfExecutions) {
		this.numOfExecutions = numOfExecutions;
	}

	public void setQueriesBeforeTest(int queriesBeforeTest) {
		this.queriesBeforeTest = queriesBeforeTest;
	}

	@Override
	public String toString() {
		return "BenchmarkBaseConfigFile [enabled=" + enabled + ", query=" + query + ", numOfExecutions="
				+ numOfExecutions + ", queriesBeforeTest=" + queriesBeforeTest + "]";
	}

}
