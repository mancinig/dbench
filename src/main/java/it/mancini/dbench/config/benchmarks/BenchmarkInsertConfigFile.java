package it.mancini.dbench.config.benchmarks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "bench.insert")
public class BenchmarkInsertConfigFile extends BenchmarkBaseConfigFile implements BenchmarkInsertConfig {
	private int commitAfter;

	@Override
	public int getCommitAfter() {
		return commitAfter;
	}

	public void setCommitAfter(int commitAfter) {
		this.commitAfter = commitAfter;
	}

	@Override
	public String toString() {
		return "BenchmarkInsertConfigFile [commitAfter=" + commitAfter + ", toString()=" + super.toString() + "]";
	}

}
