package it.mancini.dbench.config.benchmarks;

public interface BenchmarkBaseConfig {

	boolean isEnabled();

	String getQuery();

	int getNumOfExecutions();

	int getQueriesBeforeTest();

}
