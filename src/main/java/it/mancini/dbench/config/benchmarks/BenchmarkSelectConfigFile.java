package it.mancini.dbench.config.benchmarks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "bench.select")
public class BenchmarkSelectConfigFile extends BenchmarkBaseConfigFile implements BenchmarkSelectConfig {

}
