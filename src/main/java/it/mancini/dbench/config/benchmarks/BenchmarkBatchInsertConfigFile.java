package it.mancini.dbench.config.benchmarks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "bench.batchinsert")
public class BenchmarkBatchInsertConfigFile extends BenchmarkBaseConfigFile implements BenchmarkBatchInsertConfig {

	private int batchSize;

	@Override
	public int getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}

	@Override
	public String toString() {
		return "BenchmarkBatchInsertConfigFile [batchSize=" + batchSize + ", toString()=" + super.toString() + "]";
	}

}
