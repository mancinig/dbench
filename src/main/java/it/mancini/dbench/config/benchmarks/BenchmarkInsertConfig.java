package it.mancini.dbench.config.benchmarks;

public interface BenchmarkInsertConfig extends BenchmarkBaseConfig {
	int getCommitAfter();
}
