package it.mancini.dbench.config.benchmarks;

public interface BenchmarkBatchInsertConfig extends BenchmarkBaseConfig {
	int getBatchSize();
}
