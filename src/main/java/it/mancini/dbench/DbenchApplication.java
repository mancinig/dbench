package it.mancini.dbench;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.mancini.dbench.exceptions.BenchmarkExecutionException;
import it.mancini.dbench.exceptions.BenchmarkNotEnabledException;
import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.exceptions.UnsupportedDMLOperationException;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.commons.ResultWriterType;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.services.resultwriters.BenchmarkResultWriterFactory;

@SpringBootApplication
public class DbenchApplication implements CommandLineRunner {

	private Logger logger = LoggerFactory.getLogger(DbenchApplication.class.getName());

	@Autowired
	private BenchmarksManager manager;

	@Autowired
	private BenchmarkResultWriterFactory benchmarkResultWriterFactory;

	public static void main(String[] args) {
		SpringApplication.run(DbenchApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			doBenchmarks();
		} catch (DbenchException ex) {
			logger.error(ex.getMessage());
		} catch (Exception ex) {
			logger.error("Error executing request", ex);
		}
	}

	private <T extends BenchmarkBaseRequest> void doBenchmarks()
			throws UnsupportedDMLOperationException, BenchmarkNotEnabledException, BenchmarkExecutionException {

		// Performs benchmarks for each available operation
		for (DMLOperation operation : DMLOperation.values()) {
			try {
				BenchmarkResult<T> result = manager.executeBenchmark(operation);

				// Writes the results
				for (ResultWriterType writerType : ResultWriterType.values()) {
					benchmarkResultWriterFactory.getWriter(writerType).write(result);
				}

				logger.info("All done!");
			} catch (UnsupportedDMLOperationException ex) {
				logger.debug("{} benchmark not enabled", operation);
			} catch (DbenchException ex) {
				logger.error("Error running benchmark " + operation, ex);
			}
		}
	}

	private void writeInsert() throws IOException {
		// populate dummy DB
		BufferedWriter writer = new BufferedWriter(new FileWriter("insert.sql"));
		for (int i = 1; i < 10000; i++) {
			String query = String.format(
					"INSERT INTO public.users(username, password, email, created_at) VALUES ('u%d', 'pu%d', 'eu%d', '2024-03-02');\n",
					i, i, i);

			writer.write(query);
		}

		writer.close();
	}

}
