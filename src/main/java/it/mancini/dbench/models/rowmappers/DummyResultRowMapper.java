package it.mancini.dbench.models.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DummyResultRowMapper implements RowMapper<Object> {
	
	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		return null;
	}
}
