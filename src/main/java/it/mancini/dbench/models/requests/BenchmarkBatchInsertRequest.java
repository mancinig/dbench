package it.mancini.dbench.models.requests;

import it.mancini.dbench.config.benchmarks.BenchmarkBatchInsertConfig;
import it.mancini.dbench.models.commons.DMLOperation;

public class BenchmarkBatchInsertRequest extends BenchmarkBaseRequest {

	private final BenchmarkBatchInsertConfig config;

	public BenchmarkBatchInsertRequest(BenchmarkBatchInsertConfig config) {
		super(DMLOperation.BATCH_INSERT);
		this.config = config;
	}

	public BenchmarkBatchInsertConfig getConfig() {
		return config;
	}

	@Override
	public String toString() {
		return "BenchmarkBatchInsertRequest [\nconfig=" + config + ",\nBaseRequest=" + super.toString() + "\n]";
	}

}
