package it.mancini.dbench.models.requests;

import it.mancini.dbench.config.benchmarks.BenchmarkSelectConfig;
import it.mancini.dbench.models.commons.DMLOperation;

public class BenchmarkSelectRequest extends BenchmarkBaseRequest {

	private final BenchmarkSelectConfig config;

	public BenchmarkSelectRequest(BenchmarkSelectConfig config) {
		super(DMLOperation.SELECT);
		this.config = config;
	}

	@Override
	public BenchmarkSelectConfig getConfig() {
		return config;
	}

	@Override
	public String toString() {
		return "BenchmarkSelectRequest [\nconfig=" + config + ",\nBaseRequest=" + super.toString() + "\n]";
	}

}
