package it.mancini.dbench.models.requests;

import it.mancini.dbench.config.benchmarks.BenchmarkInsertConfig;
import it.mancini.dbench.models.commons.DMLOperation;

public class BenchmarkInsertRequest extends BenchmarkBaseRequest {

	private final BenchmarkInsertConfig config;

	public BenchmarkInsertRequest(BenchmarkInsertConfig config) {
		super(DMLOperation.INSERT);
		this.config = config;
	}

	public BenchmarkInsertConfig getConfig() {
		return config;
	}

	@Override
	public String toString() {
		return "BenchmarkInsertRequest [\nconfig=" + config + ",\nBaseRequest=" + super.toString() + "\n]";
	}

}
