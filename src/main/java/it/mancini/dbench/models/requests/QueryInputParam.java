package it.mancini.dbench.models.requests;

import it.mancini.dbench.models.commons.InsertParamType;

public class QueryInputParam {
	private InsertParamType type;
	private boolean random;
	private int lenght;
	private String stringValue;
	private int intValue;

	private QueryInputParam() {

	}

	public static QueryInputParam buildRandomString(int lenght) {
		QueryInputParam param = new QueryInputParam();
		param.type = InsertParamType.STRING;
		param.random = true;
		param.lenght = lenght;
		return param;
	}

	public static QueryInputParam buildRandomDate() {
		QueryInputParam param = new QueryInputParam();
		param.type = InsertParamType.DATE;
		param.random = true;
		return param;
	}

	public static QueryInputParam buildFixedString(String stringValue) {
		QueryInputParam param = new QueryInputParam();
		param.type = InsertParamType.STRING;
		param.stringValue = stringValue;
		return param;
	}

	public static QueryInputParam buildFixedInt(int intValue) {
		QueryInputParam param = new QueryInputParam();
		param.type = InsertParamType.INT;
		param.intValue = intValue;
		return param;
	}

	public InsertParamType getType() {
		return type;
	}

	public boolean isRandom() {
		return random;
	}

	public int getLenght() {
		return lenght;
	}

	public String getStringValue() {
		return stringValue;
	}

	public int getIntValue() {
		return intValue;
	}

	@Override
	public String toString() {
		return "InsertParam [type=" + type + ", random=" + random + ", lenght=" + lenght + ", stringValue="
				+ stringValue + "]";
	}

}
