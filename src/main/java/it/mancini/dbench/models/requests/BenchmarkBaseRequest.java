package it.mancini.dbench.models.requests;

import it.mancini.dbench.config.benchmarks.BenchmarkBaseConfig;
import it.mancini.dbench.models.commons.DMLOperation;

public abstract class BenchmarkBaseRequest {

	public abstract BenchmarkBaseConfig getConfig();
	
	private final DMLOperation operation;

	protected BenchmarkBaseRequest(DMLOperation operation) {
		this.operation = operation;
	}

	public DMLOperation getOperation() {
		return operation;
	}

	@Override
	public String toString() {
		return "BenchmarkBaseRequest [operation=" + operation + "]";
	}

}
