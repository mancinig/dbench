package it.mancini.dbench.models.requests;

import java.util.List;

public class QueryToRun {
	private final String query;
	private final List<QueryInputParam> params;

	public QueryToRun(String query, List<QueryInputParam> params) {
		super();
		this.query = query;
		this.params = params;
	}

	public String getQuery() {
		return query;
	}

	public List<QueryInputParam> getParams() {
		return params;
	}

}
