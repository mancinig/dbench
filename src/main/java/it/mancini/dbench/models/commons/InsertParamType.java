package it.mancini.dbench.models.commons;

public enum InsertParamType {
	STRING, DATE, INT;
}
