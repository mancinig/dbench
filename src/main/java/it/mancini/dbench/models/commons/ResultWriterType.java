package it.mancini.dbench.models.commons;

public enum ResultWriterType {
	CONSOLE, FILE;
}
