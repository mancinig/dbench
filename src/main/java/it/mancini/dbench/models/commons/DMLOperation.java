package it.mancini.dbench.models.commons;

public enum DMLOperation {
	INSERT, BATCH_INSERT, SELECT, DELETE;

}
