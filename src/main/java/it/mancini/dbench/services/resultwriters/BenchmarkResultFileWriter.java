package it.mancini.dbench.services.resultwriters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mancini.dbench.config.resultwriters.BenchmarkResultConsoleFileConfig;
import it.mancini.dbench.config.resultwriters.BenchmarkResultWriterConfig;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.commons.ResultWriterType;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;

@Service
public class BenchmarkResultFileWriter extends BenchmarkResultBaseWriter {

	private Logger logger = LoggerFactory.getLogger(BenchmarkResultFileWriter.class.getName());

	@Autowired
	private BenchmarkResultConsoleFileConfig config;

	@Override
	protected <T extends BenchmarkBaseRequest> void writeBaseResults(BenchmarkResult<T> result) {

		try {
			createFolderIfNotExists();
			String filePath = getFileFullPath(result.getRequest().getOperation());
			BufferedWriter writer = getWriter(filePath);

			writer.write("# REQUEST");
			writer.write("\n\n```\n");
			writer.write(result.getRequest().toString());
			writer.write("\n```\n\n");
			writer.write("# RESULTS\n");
			writer.write("- " + getFormattedResult(result.getMin(), "**MIN**"));
			writer.write("\n");
			writer.write("- " + getFormattedResult(result.getMax(), "**MAX**"));
			writer.write("\n");
			writer.write("- " + getFormattedResult(result.getAvg(), "**AVG**"));

			writer.close();

			logger.info("Result written in {} file.", filePath);
		} catch (Exception e) {
			logger.error("Error writing results", e);
		}

	}

	@Override
	protected <T extends BenchmarkBaseRequest> void writeExecutionsResults(BenchmarkResult<T> result) {
		String filePath = getFileFullPath(result.getRequest().getOperation());

		try (BufferedWriter writer = getWriter(filePath)) {
			writer.write("\n\n");
			writer.write("## Executions\n");
			List<Long> executionTimes = result.getExecutionTimes();
			int i = 1;
			for (Long time : executionTimes) {
				String prefix = "- Execution `" + i++ + "` ";
				writer.write(getFormattedResult(time, prefix));
				writer.write("\n\n");
			}
		} catch (Exception e) {
			logger.error("Error writing results", e);
		}

	}

	private void createFolderIfNotExists() throws IOException {
		String folder = config.getFolder();
		Path path = Paths.get(folder);
		if (!Files.isDirectory(path)) {
			Files.createDirectory(path);
		}
	}

	private BufferedWriter getWriter(String filePath) throws IOException {
		return new BufferedWriter(new FileWriter(filePath, true));
	}

	private String getFileFullPath(DMLOperation operation) {
		String fileName = getFileName(operation);
		return config.getFolder() + File.separator + fileName;
	}

	private String getFileName(DMLOperation operation) {
		return String.format("%s_%s_benchmark.md", getFormattedDate(), operation.toString());
	}

	private String getFormattedDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(config.getDateFormat());
		LocalDateTime ldt = LocalDateTime.now();
		return ldt.format(formatter);
	}

	@Override
	public ResultWriterType getType() {
		return ResultWriterType.FILE;
	}

	@Override
	protected BenchmarkResultWriterConfig getConfig() {
		return config;
	}

}
