package it.mancini.dbench.services.resultwriters;

import it.mancini.dbench.models.commons.ResultWriterType;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;

public interface BenchmarkResultWriter {

	<T extends BenchmarkBaseRequest> void write(BenchmarkResult<T> result);

	boolean isEnabled();

	ResultWriterType getType();

}
