package it.mancini.dbench.services.resultwriters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.mancini.dbench.config.resultwriters.BenchmarkResultWriterConfig;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;

public abstract class BenchmarkResultBaseWriter implements BenchmarkResultWriter {

	private Logger logger = LoggerFactory.getLogger(BenchmarkResultBaseWriter.class.getName());

	protected abstract <T extends BenchmarkBaseRequest> void writeBaseResults(BenchmarkResult<T> result);

	protected abstract <T extends BenchmarkBaseRequest> void writeExecutionsResults(BenchmarkResult<T> result);

	protected abstract BenchmarkResultWriterConfig getConfig();

	@Override
	public <T extends BenchmarkBaseRequest> void write(BenchmarkResult<T> result) {

		if (!isEnabled()) {
			// Nothing to do
			logger.info("{} writer is not enabled!", getType());
			return;
		}

		writeBaseResults(result);

		if (getConfig().isWriteExecutions()) {
			writeExecutionsResults(result);
		} else {
			logger.info("Write executions disable for {} writer!", getType());
		}
	}

	protected String getFormattedResult(long time, String prefix) {
		return String.format("%s (%s): %f", prefix, getConfig().getUnit(), time / getConfig().getTimeFactor());
	}

	@Override
	public boolean isEnabled() {
		return getConfig().isEnabled();
	}

}
