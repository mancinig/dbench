package it.mancini.dbench.services.resultwriters;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.mancini.dbench.models.commons.ResultWriterType;

@Component
public class BenchmarkResultWriterFactory {
	private Map<ResultWriterType, BenchmarkResultWriter> writers;

	@Autowired
	public BenchmarkResultWriterFactory(List<BenchmarkResultWriter> writers) {
		this.writers = writers.stream()
				.collect(Collectors.toUnmodifiableMap(BenchmarkResultWriter::getType, Function.identity()));
	}

	public BenchmarkResultWriter getWriter(ResultWriterType type) {
		return writers.get(type);
	}
}
