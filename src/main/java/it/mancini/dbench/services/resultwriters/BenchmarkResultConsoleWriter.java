package it.mancini.dbench.services.resultwriters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.mancini.dbench.config.resultwriters.BenchmarkResultConsoleWriterConfig;
import it.mancini.dbench.config.resultwriters.BenchmarkResultWriterConfig;
import it.mancini.dbench.models.commons.ResultWriterType;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;

@Service
public class BenchmarkResultConsoleWriter extends BenchmarkResultBaseWriter {

	private Logger logger = LoggerFactory.getLogger(BenchmarkResultConsoleWriter.class.getName());

	@Autowired
	private BenchmarkResultConsoleWriterConfig config;

	@Override
	protected <T extends BenchmarkBaseRequest> void writeBaseResults(BenchmarkResult<T> result) {
		logger.info("");
		logger.info("*********************");
		logger.info("---*** REQUEST ***---");
		logger.info(result.getRequest().toString());
		logger.info("---------------------");
		logger.info("");
		logger.info("---*** RESULTS ***---");
		logger.info(getFormattedResult(result.getMin(), "MIN"));
		logger.info(getFormattedResult(result.getMax(), "MAX"));
		logger.info(getFormattedResult(result.getAvg(), "AVG"));
		logger.info("---------------------");
		logger.info("*********************");
		logger.info("");
	}

	@Override
	protected <T extends BenchmarkBaseRequest> void writeExecutionsResults(BenchmarkResult<T> result) {
		logger.info("---*** EX TIME ***---");
		List<Long> executionTimes = result.getExecutionTimes();
		int i = 1;
		for (Long time : executionTimes) {
			String prefix = "Execution " + i++ + "		";
			logger.info(getFormattedResult(time, prefix));
		}
		logger.info("---------------------");
		logger.info("*********************");
	}

	@Override
	public ResultWriterType getType() {
		return ResultWriterType.CONSOLE;
	}

	@Override
	protected BenchmarkResultWriterConfig getConfig() {
		return config;
	}

}
