package it.mancini.dbench.services.benchmarks;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.mancini.dbench.exceptions.UnsupportedDMLOperationException;
import it.mancini.dbench.models.commons.DMLOperation;

@Component
public class BenchmarkFactory {
	private Map<DMLOperation, Benchmark> benchmarkProviders;

	@Autowired
	public BenchmarkFactory(List<Benchmark> providers) {
		this.benchmarkProviders = providers.stream()
				.collect(Collectors.toUnmodifiableMap(Benchmark::getOperation, Function.identity()));
	}

	public Benchmark getBenchmarkProvider(DMLOperation operation)
			throws UnsupportedDMLOperationException {

		if (!benchmarkProviders.containsKey(operation)) {
			throw new UnsupportedDMLOperationException(operation);
		}

		return benchmarkProviders.get(operation);
	}
}
