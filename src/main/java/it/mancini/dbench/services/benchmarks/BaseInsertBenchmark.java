package it.mancini.dbench.services.benchmarks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import it.mancini.dbench.models.requests.BenchmarkBaseRequest;

public abstract class BaseInsertBenchmark<T extends BenchmarkBaseRequest> extends BaseBenchmark<T> {

	private Logger logger = LoggerFactory.getLogger(BaseInsertBenchmark.class.getName());

	@Autowired
	private PlatformTransactionManager transactionManager;

	protected TransactionStatus initTransaction() {
		return transactionManager.getTransaction(new DefaultTransactionDefinition());
	}

	protected TransactionStatus doCommitAndRestart(TransactionStatus transactionStatus) {
		transactionManager.commit(transactionStatus);
		return transactionManager.getTransaction(new DefaultTransactionDefinition());
	}

	protected void doCommit(TransactionStatus transactionStatus) {
		if (transactionStatus != null && !transactionStatus.isCompleted()) {
			logger.debug("Doing last commit!");
			transactionManager.commit(transactionStatus);
		}
	}

}
