package it.mancini.dbench.services.benchmarks;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import it.mancini.dbench.config.benchmarks.BenchmarkSelectConfig;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkSelectRequest;
import it.mancini.dbench.models.requests.QueryToRun;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.models.rowmappers.DummyResultRowMapper;

@Service
public class SelectBenchmark extends BaseBenchmark<BenchmarkSelectRequest> {

	private Logger logger = LoggerFactory.getLogger(SelectBenchmark.class.getName());

	@Override
	public BenchmarkResult<BenchmarkSelectRequest> startBenchmark(BenchmarkSelectRequest request, QueryToRun queryToRun)
			throws SQLException {

		logger.debug("Request: {}", request);

		BenchmarkSelectConfig config = request.getConfig();

		int numOfExecutions = config.getNumOfExecutions();

		BenchmarkResult<BenchmarkSelectRequest> result = buildResult(request);

		Object[] params = buildParams(queryToRun.getParams());

		RowMapper<Object> rowMapper = buildRowMapper();
		for (int i = 0; i < numOfExecutions; i++) {
			long startTime = System.nanoTime();
			jdbcTemplate.queryForObject(queryToRun.getQuery(), rowMapper, params);
			long endTime = System.nanoTime();

			long executionTime = endTime - startTime;

			updateResult(result, executionTime);
		}

		return result;
	}

	@Override
	protected <Z> void executeQuery(String query, Object[] params, RowMapper<Z> rowMapper) {
		jdbcTemplate.queryForObject(query, rowMapper, params);
	}

	@Override
	protected RowMapper<Object> buildRowMapper() {
		return new DummyResultRowMapper();
	}

	@Override
	public DMLOperation getOperation() {
		return DMLOperation.SELECT;
	}

}
