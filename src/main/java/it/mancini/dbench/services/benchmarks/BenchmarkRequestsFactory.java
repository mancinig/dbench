package it.mancini.dbench.services.benchmarks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.mancini.dbench.config.benchmarks.BenchmarkBatchInsertConfig;
import it.mancini.dbench.config.benchmarks.BenchmarkInsertConfig;
import it.mancini.dbench.config.benchmarks.BenchmarkSelectConfig;
import it.mancini.dbench.exceptions.UnsupportedDMLOperationException;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.requests.BenchmarkBatchInsertRequest;
import it.mancini.dbench.models.requests.BenchmarkInsertRequest;
import it.mancini.dbench.models.requests.BenchmarkSelectRequest;

@Component
public class BenchmarkRequestsFactory {

	@Autowired
	private BenchmarkSelectConfig benchmarkSelectConfig;

	@Autowired
	private BenchmarkInsertConfig benchmarkInsertConfig;

	@Autowired
	private BenchmarkBatchInsertConfig benchmarkBatchInsertConfig;

	public BenchmarkBaseRequest buildRequestFromConfig(DMLOperation operation) throws UnsupportedDMLOperationException {
		switch (operation) {
		case INSERT:
			return buildInsertRequestFromConfig();
		case BATCH_INSERT:
			return buildBatchInsertRequestFromConfig();
		case SELECT:
			return buildSelectRequestFromConfig();

		default:
			throw new UnsupportedDMLOperationException(operation);
		}
	}

	private BenchmarkBaseRequest buildInsertRequestFromConfig() {
		return new BenchmarkInsertRequest(benchmarkInsertConfig);
	}

	private BenchmarkBaseRequest buildBatchInsertRequestFromConfig() {
		return new BenchmarkBatchInsertRequest(benchmarkBatchInsertConfig);
	}

	private BenchmarkSelectRequest buildSelectRequestFromConfig() {
		return new BenchmarkSelectRequest(benchmarkSelectConfig);
	}

}
