package it.mancini.dbench.services.benchmarks;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import it.mancini.dbench.config.benchmarks.BenchmarkInsertConfig;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkInsertRequest;
import it.mancini.dbench.models.requests.QueryToRun;
import it.mancini.dbench.models.results.BenchmarkResult;

@Service
public class InsertBenchmark extends BaseInsertBenchmark<BenchmarkInsertRequest> {

	private Logger logger = LoggerFactory.getLogger(InsertBenchmark.class.getName());

	@Override
	public BenchmarkResult<BenchmarkInsertRequest> startBenchmark(BenchmarkInsertRequest request, QueryToRun queryToRun)
			throws SQLException {
		logger.debug("Request: {}", request);

		BenchmarkInsertConfig config = request.getConfig();

		int numOfExecutions = config.getNumOfExecutions();

		// if commitAfter is not specified, commits to each query
		int commitAfter = config.getCommitAfter() > 0 ? config.getCommitAfter() : 1;

		BenchmarkResult<BenchmarkInsertRequest> result = buildResult(request);

		TransactionStatus transactionStatus = initTransaction();

		for (int i = 0; i < numOfExecutions; i++) {

			Object[] params = buildParams(queryToRun.getParams());

			long startTime = System.nanoTime();
			jdbcTemplate.update(queryToRun.getQuery(), params);
			long endTime = System.nanoTime();

			long executionTime = endTime - startTime;

			updateResult(result, executionTime);

			if (i > 0 && i % commitAfter == 0) {
				logger.debug("Execution {}. Doing commit!", i);
				transactionStatus = doCommitAndRestart(transactionStatus);
			}
		}

		doCommit(transactionStatus);

		return result;
	}

	@Override
	protected <Z> void executeQuery(String query, Object[] params, RowMapper<Z> rowMapper) {
		jdbcTemplate.update(query, params);
	}

	@Override
	public DMLOperation getOperation() {
		return DMLOperation.INSERT;
	}
}
