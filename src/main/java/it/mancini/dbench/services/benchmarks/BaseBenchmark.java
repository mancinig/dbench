package it.mancini.dbench.services.benchmarks;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import it.mancini.dbench.config.benchmarks.BenchmarkBaseConfig;
import it.mancini.dbench.exceptions.BenchmarkConfigurationException;
import it.mancini.dbench.exceptions.BenchmarkExecutionException;
import it.mancini.dbench.exceptions.BenchmarkNotEnabledException;
import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.requests.QueryInputParam;
import it.mancini.dbench.models.requests.QueryToRun;
import it.mancini.dbench.models.results.BenchmarkResult;

public abstract class BaseBenchmark<T extends BenchmarkBaseRequest> implements Benchmark<T> {

	protected abstract BenchmarkResult<T> startBenchmark(T request, QueryToRun queryToRun) throws SQLException;

	protected abstract <Z> void executeQuery(String query, Object[] params, RowMapper<Z> rowMapper);

	private static final long MAX_EXECUTIONS = 99999;

	private Logger logger = LoggerFactory.getLogger(BenchmarkBaseRequest.class.getName());

	@Autowired
	protected JdbcTemplate jdbcTemplate;

	@Override
	public BenchmarkResult<T> execute(T request) throws DbenchException {
		try {
			// check whether the bench is enabled
			if (!isEnabled(request)) {
				throw new BenchmarkNotEnabledException(getOperation());
			}

			BenchmarkBaseConfig config = request.getConfig();

			long numOfExecutions = config.getNumOfExecutions();
			if (numOfExecutions > MAX_EXECUTIONS || numOfExecutions < 0) {
				throw new BenchmarkConfigurationException(getOperation(), String.format(
						"numOfExecution should be between 1 and %d. Check your configuration file!", MAX_EXECUTIONS));
			}

			logger.info("Starting {} benchmark!", getOperation());

			// retrieves the query and input params
			QueryToRun queryToRun = getQueryToRun(config);

			// executes the initial queries
			runQueriesBeforeTest(queryToRun, config);

			// starts the real test
			BenchmarkResult<T> result = startBenchmark(request, queryToRun);

			// calculates the average base on the execution results.
			calculateAvg(result);

			return result;

		} catch (DbenchException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error("Error running benchmark!", ex);
			throw new BenchmarkExecutionException(getOperation(), ex.getMessage());
		}
	}

	public QueryToRun getQueryToRun(BenchmarkBaseConfig config) {
		// TODO to improve
		/*
		 * Is responsible for processing a query template and constructing the actual
		 * query to be executed during benchmarking. It dynamically replaces
		 * placeholders in the input query with appropriate values based on the provided
		 * configuration.
		 * 
		 * It defines a regular expression pattern to identify placeholders within the
		 * query. These placeholders represent different types of input parameters (see
		 * documentation for a complete list).
		 * 
		 * The final query is constructed by replacing all placeholders with the ?
		 * character (used for parameter binding in prepared statements).
		 */

		String query = config.getQuery();

		Pattern pattern = Pattern
				.compile("(\\[RANDOM_STRING_[^\\]]+\\])|(\\[RANDOM_DATE\\])|(\\[INT_[^\\]]+\\])|(\\['[^\\\\']+'\\])");
		Matcher matcher = pattern.matcher(query);

		List<QueryInputParam> inputParams = new LinkedList<>();
		String parsedQuery = matcher.replaceAll(matchResult -> {

			if (matchResult.group(1) != null) {
				String randomStringGroup = matchResult.group(1).replace("[", "").replace("]", "");
				String params[] = randomStringGroup.split("_");
				int lenght = Integer.valueOf(params[2]);
				inputParams.add(QueryInputParam.buildRandomString(lenght));
			} else if (matchResult.group(2) != null) {
				inputParams.add(QueryInputParam.buildRandomDate());
			} else if (matchResult.group(3) != null) {
				String fixedInt = matchResult.group(3);
				fixedInt = fixedInt.replace("[INT_", "").replace("]", "");
				inputParams.add(QueryInputParam.buildFixedInt(Integer.valueOf(fixedInt)));
			} else {
				String fixedString = matchResult.group(4);
				fixedString = fixedString.replace("['", "").replace("']", "");
				inputParams.add(QueryInputParam.buildFixedString(fixedString));
			}
			return "?";
		});
		logger.debug("Query to run: {}", parsedQuery);
		inputParams.forEach(p -> {
			logger.debug("Parameter: {}", p.toString());
		});

		return new QueryToRun(parsedQuery, inputParams);
	}

	public Object[] buildParams(List<QueryInputParam> insertParams) {
		return insertParams.stream().map(insertParam -> buildParam(insertParam)).toArray();
	}

	public Object buildParam(QueryInputParam insertParam) {
		/*
		 * Converts a QueryInputParameter into the corresponding object to be used as an
		 * input parameter for the query.
		 */
		boolean random = insertParam.isRandom();

		switch (insertParam.getType()) {
		case STRING:
			if (random) {
				return RandomStringUtils.random(insertParam.getLenght(), true, true);
			}
			return insertParam.getStringValue();
		case DATE:
			return LocalDateTime.now();
		case INT:
			return insertParam.getIntValue();
		}

		return null;
	}

	private <Y> void runQueriesBeforeTest(QueryToRun queryToRun, BenchmarkBaseConfig config) {
		/*
		 * Executes, if set in the configuration, some initial queries.
		 * 
		 * It is useful to avoid wrong results due to the first connection to the
		 * database.
		 */
		int queriesBeforeTest = config.getQueriesBeforeTest();
		logger.debug("Running {} queries before test.", queriesBeforeTest);

		RowMapper<Object> rowMapper = buildRowMapper();

		for (int i = 0; i < queriesBeforeTest; i++) {
			Object[] params = buildParams(queryToRun.getParams());
			executeQuery(queryToRun.getQuery(), params, rowMapper);
		}
	}

	protected RowMapper<Object> buildRowMapper() {
		return null;
	}

	protected BenchmarkResult<T> buildResult(T request) {
		return new BenchmarkResult<T>(request, Long.MAX_VALUE, Long.MIN_VALUE, 0,
				new ArrayList<>(request.getConfig().getNumOfExecutions()));
	}

	public void updateResult(BenchmarkResult<T> result, long executionTime) {
		// updates MIN and MAX values and adds the execution time to the result list
		result.setMin(Math.min(result.getMin(), executionTime));
		result.setMax(Math.max(result.getMax(), executionTime));
		result.getExecutionTimes().add(executionTime);
	}

	private void calculateAvg(BenchmarkResult<T> result) {
		result.setAvg((long) result.getExecutionTimes().stream().mapToLong(Long::longValue).average().getAsDouble());
	}

	@Override
	public boolean isEnabled(T request) {
		return request.getConfig().isEnabled();
	}

}
