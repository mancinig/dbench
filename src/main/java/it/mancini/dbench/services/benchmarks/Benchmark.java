package it.mancini.dbench.services.benchmarks;

import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.results.BenchmarkResult;

public interface Benchmark<T extends BenchmarkBaseRequest> {

	BenchmarkResult<T> execute(T request) throws DbenchException;

	DMLOperation getOperation();

	boolean isEnabled(T request);

}
