package it.mancini.dbench.services.benchmarks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import it.mancini.dbench.config.benchmarks.BenchmarkBatchInsertConfig;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkBatchInsertRequest;
import it.mancini.dbench.models.requests.QueryToRun;
import it.mancini.dbench.models.results.BenchmarkResult;

@Service
public class BatchInsertBenchmark extends BaseInsertBenchmark<BenchmarkBatchInsertRequest> {

	private Logger logger = LoggerFactory.getLogger(BatchInsertBenchmark.class.getName());

	@Override
	public BenchmarkResult<BenchmarkBatchInsertRequest> startBenchmark(BenchmarkBatchInsertRequest request, QueryToRun queryToRun)
			throws SQLException {
		logger.debug("Request: {}", request);

		BenchmarkBatchInsertConfig config = request.getConfig();

		int numOfExecutions = config.getNumOfExecutions();

		// if batchSize is not specified, batch size = 1
		int batchSize = config.getBatchSize() > 0 ? config.getBatchSize() : 1;

		BenchmarkResult<BenchmarkBatchInsertRequest> result = buildResult(request);

		TransactionStatus transactionStatus = initTransaction();

		for (int i = 0; i < numOfExecutions; i += batchSize) {
			logger.debug("Running insert from {} to {}", i, i + batchSize);
			
			//building list of parameters
			List<Object[]> paramsList = new ArrayList<>(batchSize);
			for (int j = 0; j < batchSize; j++) {
				paramsList.add(buildParams(queryToRun.getParams()));
			}
			
			//execute
			long startTime = System.nanoTime();
			jdbcTemplate.batchUpdate(queryToRun.getQuery(), paramsList);
			long endTime = System.nanoTime();

			//this is the execution time to execute batchSize queries.
			long executionTime = (endTime - startTime) / batchSize;
			
			transactionStatus = doCommitAndRestart(transactionStatus);

			updateResult(result, executionTime);
		}

		return result;
	}

	@Override
	protected <Z> void executeQuery(String query, Object[] params, RowMapper<Z> rowMapper) {
		jdbcTemplate.batchUpdate(query, Collections.singletonList(params));
	}

	@Override
	public DMLOperation getOperation() {
		return DMLOperation.BATCH_INSERT;
	}
}
