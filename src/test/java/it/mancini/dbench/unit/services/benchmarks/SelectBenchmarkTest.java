package it.mancini.dbench.unit.services.benchmarks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import it.mancini.dbench.config.benchmarks.BenchmarkSelectConfig;
import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.models.requests.BenchmarkSelectRequest;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.services.benchmarks.SelectBenchmark;

@ExtendWith(MockitoExtension.class)
public class SelectBenchmarkTest {

	@InjectMocks
	private SelectBenchmark selectService;

	@Mock
	private BenchmarkSelectConfig config;

	@Mock
	private JdbcTemplate jdbcTemplate;

	private BenchmarkSelectRequest request;

	@BeforeEach
	void setUp() {
		request = new BenchmarkSelectRequest(config);
	}

	@Test
	void testStartBenchmark() throws DbenchException {
		// Mock input parameters
		int numOfExecutions = 100;
		when(config.isEnabled()).thenReturn(true);
		when(config.getNumOfExecutions()).thenReturn(numOfExecutions);
		when(config.getQuery()).thenReturn("SELECT * FROM users WHERE id = [INT_28]");

		// Call the method
		BenchmarkResult<BenchmarkSelectRequest> result = selectService.execute(request);

		// Verify that the result is not null
		assertNotNull(result);
		assertEquals(numOfExecutions, result.getExecutionTimes().size());
		assertEquals(request, result.getRequest());
	}
}
