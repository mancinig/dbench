package it.mancini.dbench.unit.services.benchmarks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import it.mancini.dbench.config.benchmarks.BenchmarkInsertConfig;
import it.mancini.dbench.exceptions.BenchmarkConfigurationException;
import it.mancini.dbench.exceptions.BenchmarkNotEnabledException;
import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.models.requests.BenchmarkInsertRequest;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.services.benchmarks.InsertBenchmark;

@ExtendWith(MockitoExtension.class)
public class InsertBenchmarkTest {
	
	@InjectMocks
	private InsertBenchmark insertBenchmark;
	
	@Mock
    private BenchmarkInsertConfig config;
	
	@Mock
	private JdbcTemplate jdbcTemplate;

	@Mock
	private PlatformTransactionManager transactionManager;
	
	private BenchmarkInsertRequest request;

    @BeforeEach
    void setUp() {
        request = new BenchmarkInsertRequest(config);
    }

    @Test
    void testExecute_BenchmarkNotEnabled() {
        when(config.isEnabled()).thenReturn(false);

        assertThrows(BenchmarkNotEnabledException.class, () -> insertBenchmark.execute(request));
    }

    @Test
    void testExecute_InvalidNumOfExecutions() {
        when(config.isEnabled()).thenReturn(true);
        when(config.getNumOfExecutions()).thenReturn(-1);

        assertThrows(BenchmarkConfigurationException.class, () -> insertBenchmark.execute(request));
    }

    @Test
    void testExecute_TooManyExecutions() {
        when(config.isEnabled()).thenReturn(true);
        when(config.getNumOfExecutions()).thenReturn(1000000);

        assertThrows(BenchmarkConfigurationException.class, () -> insertBenchmark.execute(request));
    }

    @Test
    void testExecute_ValidExecution() throws DbenchException {
    	int numOfExecutions = 100;
        when(config.isEnabled()).thenReturn(true);
        when(config.getNumOfExecutions()).thenReturn(numOfExecutions);
        when(config.getQuery()).thenReturn("select * from users where id = [INT_1];");

        BenchmarkResult<?> result = insertBenchmark.execute(request);

        assertNotNull(result);
        assertEquals(numOfExecutions, result.getExecutionTimes().size());
		assertEquals(request, result.getRequest());
    }
}
