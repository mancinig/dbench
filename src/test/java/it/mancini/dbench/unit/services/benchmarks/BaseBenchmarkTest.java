package it.mancini.dbench.unit.services.benchmarks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.LinkedList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import it.mancini.dbench.config.benchmarks.BenchmarkBaseConfig;
import it.mancini.dbench.models.commons.InsertParamType;
import it.mancini.dbench.models.requests.BenchmarkSelectRequest;
import it.mancini.dbench.models.requests.QueryInputParam;
import it.mancini.dbench.models.requests.QueryToRun;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.services.benchmarks.SelectBenchmark;

@ExtendWith(MockitoExtension.class)
public class BaseBenchmarkTest {

	private SelectBenchmark baseBenchmark;
	private BenchmarkBaseConfig config;

	@BeforeEach
	public void setUp() {
		baseBenchmark = new SelectBenchmark();
		config = mock(BenchmarkBaseConfig.class);
	}

	@Test
  public void testGetQueryToRun_RandomString() {
    when(config.getQuery()).thenReturn("[RANDOM_STRING_10]");

    QueryToRun result = baseBenchmark.getQueryToRun(config);

    assertNotNull(result);
    assertTrue(result.getQuery().contains("?"));
    assertEquals(1, result.getParams().size());
    QueryInputParam param = result.getParams().get(0);
    assertTrue(param.isRandom());
    assertEquals(InsertParamType.STRING, param.getType());
  }

	@Test
  public void testGetQueryToRun_RandomDate() {
    when(config.getQuery()).thenReturn("[RANDOM_DATE]");

    QueryToRun result = baseBenchmark.getQueryToRun(config);

    assertNotNull(result);
    assertTrue(result.getQuery().contains("?"));
    assertEquals(1, result.getParams().size());
    QueryInputParam param = result.getParams().get(0);
    assertTrue(param.isRandom());
    assertEquals(InsertParamType.DATE, param.getType());
  }

	@Test
  public void testGetQueryToRun_FixedInt() {
    when(config.getQuery()).thenReturn("[INT_42]");

    QueryToRun result = baseBenchmark.getQueryToRun(config);

    assertNotNull(result);
    assertTrue(result.getQuery().contains("?"));
    assertEquals(1, result.getParams().size());
    QueryInputParam param = result.getParams().get(0);
    assertFalse(param.isRandom());
    assertEquals(InsertParamType.INT, param.getType());
    assertEquals(42, param.getIntValue());
  }

	@Test
  public void testGetQueryToRun_FixedString() {
    when(config.getQuery()).thenReturn("['Hello']");

    QueryToRun result = baseBenchmark.getQueryToRun(config);

    assertNotNull(result);
    assertTrue(result.getQuery().contains("?"));
    assertEquals(1, result.getParams().size());
    QueryInputParam param = result.getParams().get(0);
    assertFalse(param.isRandom());
    assertEquals(InsertParamType.STRING, param.getType());
    assertEquals("Hello", param.getStringValue());
  }

	@Test
	public void testGetQueryToRun_MultipleParams() {
	    when(config.getQuery()).thenReturn("SELECT * FROM users WHERE id = [INT_42] AND name = ['Ciccio']");

	    QueryToRun result = baseBenchmark.getQueryToRun(config);

	    assertNotNull(result);
	    assertTrue(result.getQuery().contains("?"));
	    assertEquals(2, result.getParams().size());
	    
	    QueryInputParam intParam = result.getParams().get(0);
	    assertFalse(intParam.isRandom());
	    assertEquals(InsertParamType.INT, intParam.getType());
	    assertEquals(42, intParam.getIntValue());
	    
	    QueryInputParam strParam = result.getParams().get(1);
	    assertFalse(strParam.isRandom());
	    assertEquals(InsertParamType.STRING, strParam.getType());
	    assertEquals("Ciccio", strParam.getStringValue());
	}

	@Test
	public void testGetQueryToRun_ZeroParams() {
	    when(config.getQuery()).thenReturn("SELECT * FROM users WHERE id = 1");

	    QueryToRun result = baseBenchmark.getQueryToRun(config);

	    assertNotNull(result);
	    assertFalse(result.getQuery().contains("?"));
	    assertEquals(0, result.getParams().size());
	}

	@Test
	void testBuildParam_RandomString() {
		QueryInputParam param = QueryInputParam.buildRandomString(10);
		Object result = baseBenchmark.buildParam(param);

		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals(10, ((String) result).length());
	}

	@Test
	void testBuildParam_NonRandomString() {
		QueryInputParam param = QueryInputParam.buildFixedString("Hello");
		Object result = baseBenchmark.buildParam(param);

		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals("Hello", result);
	}

	@Test
	void testBuildParam_Date() {
		QueryInputParam param = QueryInputParam.buildRandomDate();
		Object result = baseBenchmark.buildParam(param);

		assertNotNull(result);
		assertTrue(result instanceof LocalDateTime);
		// Add more assertions related to date handling
	}

	@Test
	void testBuildParam_Int() {
		QueryInputParam param = QueryInputParam.buildFixedInt(42);
		Object result = baseBenchmark.buildParam(param);

		assertNotNull(result);
		assertTrue(result instanceof Integer);
		assertEquals(42, result);
	}

	@Test
	void testUpdateResult_MinMax() {
		BenchmarkResult<BenchmarkSelectRequest> result = buildResult(100, 200);

		baseBenchmark.updateResult(result, 150);

		assertEquals(100, result.getMin());
		assertEquals(200, result.getMax());
	}

	@Test
	void testUpdateResult_UpdateMin() {
		BenchmarkResult<BenchmarkSelectRequest> result = buildResult(100, 200);

		baseBenchmark.updateResult(result, 80);

		assertEquals(80, result.getMin());
	}

	@Test
	void testUpdateResult_UpdateMax() {
		BenchmarkResult<BenchmarkSelectRequest> result = buildResult(100, 200);

		baseBenchmark.updateResult(result, 300);

		assertEquals(300, result.getMax());
	}

	@Test
	void testUpdateResult_AddExecutionTime() {
		BenchmarkResult<BenchmarkSelectRequest> result = buildResult(100, 200);
		baseBenchmark.updateResult(result, 120);

		assertEquals(1, result.getExecutionTimes().size());
		assertEquals(120, result.getExecutionTimes().get(0));
	}
	
	private BenchmarkResult<BenchmarkSelectRequest> buildResult(long min, long max) {
		return new BenchmarkResult(null, min, max, 0, new LinkedList());
	}

}