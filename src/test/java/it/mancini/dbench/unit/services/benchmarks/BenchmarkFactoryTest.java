package it.mancini.dbench.unit.services.benchmarks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.mancini.dbench.exceptions.UnsupportedDMLOperationException;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.services.benchmarks.BatchInsertBenchmark;
import it.mancini.dbench.services.benchmarks.Benchmark;
import it.mancini.dbench.services.benchmarks.BenchmarkFactory;
import it.mancini.dbench.services.benchmarks.InsertBenchmark;
import it.mancini.dbench.services.benchmarks.SelectBenchmark;

@ExtendWith(MockitoExtension.class)
public class BenchmarkFactoryTest {

	private BenchmarkFactory benchmarkFactory;

	@Mock
	private InsertBenchmark insertBenchmark;

	@Mock
	private SelectBenchmark selectBenchmark;

	@Mock
	private BatchInsertBenchmark batchInsertBenchmark;

	@BeforeEach
	void setUp() {
		when(insertBenchmark.getOperation()).thenReturn(DMLOperation.INSERT);

		when(selectBenchmark.getOperation()).thenReturn(DMLOperation.SELECT);

		when(batchInsertBenchmark.getOperation()).thenReturn(DMLOperation.BATCH_INSERT);

		benchmarkFactory = new BenchmarkFactory(List.of(insertBenchmark, selectBenchmark, batchInsertBenchmark));
	}

	@Test
	void testGetBenchmarkProvider_GetInsertBenchmark() throws UnsupportedDMLOperationException {
		DMLOperation operation = DMLOperation.INSERT;

		Benchmark actualBenchmark = benchmarkFactory.getBenchmarkProvider(operation);

		assertEquals(insertBenchmark, actualBenchmark);
	}

	@Test
	void testGetBenchmarkProvider_GetBatchInsertBenchmark() throws UnsupportedDMLOperationException {
		DMLOperation operation = DMLOperation.BATCH_INSERT;

		Benchmark actualBenchmark = benchmarkFactory.getBenchmarkProvider(operation);

		assertEquals(batchInsertBenchmark, actualBenchmark);
	}

	@Test
	void testGetBenchmarkProvider_GetSelectBenchmark() throws UnsupportedDMLOperationException {
		DMLOperation operation = DMLOperation.SELECT;

		Benchmark actualBenchmark = benchmarkFactory.getBenchmarkProvider(operation);

		assertEquals(selectBenchmark, actualBenchmark);
	}

	@Test
	void testGetBenchmarkProvider_InvalidOperation() {
		DMLOperation invalidOperation = DMLOperation.DELETE;

		assertThrows(UnsupportedDMLOperationException.class,
				() -> benchmarkFactory.getBenchmarkProvider(invalidOperation));
	}
}
