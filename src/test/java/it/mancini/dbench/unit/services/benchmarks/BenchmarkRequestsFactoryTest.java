package it.mancini.dbench.unit.services.benchmarks;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.mancini.dbench.config.benchmarks.BenchmarkInsertConfig;
import it.mancini.dbench.config.benchmarks.BenchmarkSelectConfig;
import it.mancini.dbench.exceptions.UnsupportedDMLOperationException;
import it.mancini.dbench.models.commons.DMLOperation;
import it.mancini.dbench.models.requests.BenchmarkBaseRequest;
import it.mancini.dbench.models.requests.BenchmarkBatchInsertRequest;
import it.mancini.dbench.models.requests.BenchmarkInsertRequest;
import it.mancini.dbench.models.requests.BenchmarkSelectRequest;
import it.mancini.dbench.services.benchmarks.BenchmarkRequestsFactory;

@ExtendWith(MockitoExtension.class)
public class BenchmarkRequestsFactoryTest {

	@Mock
	private BenchmarkSelectConfig benchmarkSelectConfig;

	@Mock
	private BenchmarkInsertConfig benchmarkInsertConfig;

	@InjectMocks
	private BenchmarkRequestsFactory benchmarkRequestFactory;

	@Test
	void testBuildRequestFromConfig_Insert() throws UnsupportedDMLOperationException {
		DMLOperation operation = DMLOperation.INSERT;
		BenchmarkBaseRequest request = benchmarkRequestFactory.buildRequestFromConfig(operation);
		assertTrue(request instanceof BenchmarkInsertRequest);
	}

	@Test
	void testBuildRequestFromConfig_BatchInsert() throws UnsupportedDMLOperationException {
		DMLOperation operation = DMLOperation.BATCH_INSERT;
		BenchmarkBaseRequest request = benchmarkRequestFactory.buildRequestFromConfig(operation);
		assertTrue(request instanceof BenchmarkBatchInsertRequest);
	}

	@Test
	void testBuildRequestFromConfig_Select() throws UnsupportedDMLOperationException {
		DMLOperation operation = DMLOperation.SELECT;
		BenchmarkBaseRequest request = benchmarkRequestFactory.buildRequestFromConfig(operation);
		assertTrue(request instanceof BenchmarkSelectRequest);
	}

	@Test
	void testBuildRequestFromConfig_Unsupported() {
		DMLOperation operation = DMLOperation.DELETE;
		assertThrows(UnsupportedDMLOperationException.class,
				() -> benchmarkRequestFactory.buildRequestFromConfig(operation));
	}

}
