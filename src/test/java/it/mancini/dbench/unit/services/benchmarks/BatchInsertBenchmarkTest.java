package it.mancini.dbench.unit.services.benchmarks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import it.mancini.dbench.config.benchmarks.BenchmarkBatchInsertConfig;
import it.mancini.dbench.exceptions.DbenchException;
import it.mancini.dbench.models.requests.BenchmarkBatchInsertRequest;
import it.mancini.dbench.models.results.BenchmarkResult;
import it.mancini.dbench.services.benchmarks.BatchInsertBenchmark;

@ExtendWith(MockitoExtension.class)
public class BatchInsertBenchmarkTest {

	@InjectMocks
	private BatchInsertBenchmark batchInsertBenchmark;

	@Mock
	private BenchmarkBatchInsertConfig config;

	@Mock
	private JdbcTemplate jdbcTemplate;

	@Mock
	private PlatformTransactionManager transactionManager;

	private BenchmarkBatchInsertRequest request;

	@BeforeEach
	void setUp() {
		request = new BenchmarkBatchInsertRequest(config);
	}

	@Test
	void testStartBenchmark() throws DbenchException {
		// Mock input parameters
		int numOfExecutions = 100;
		when(config.isEnabled()).thenReturn(true);
		when(config.getNumOfExecutions()).thenReturn(numOfExecutions);
		when(config.getQuery()).thenReturn(
				"INSERT INTO public.users(username, password, email, created_at) VALUES ([RANDOM_STRING_15], ['password'], [RANDOM_STRING_20], [RANDOM_DATE]);");

		// Call the method
		BenchmarkResult<BenchmarkBatchInsertRequest> result = batchInsertBenchmark.execute(request);

		// Verify that the result is not null
		assertNotNull(result);
		assertEquals(numOfExecutions, result.getExecutionTimes().size());
		assertEquals(request, result.getRequest());
	}
}
