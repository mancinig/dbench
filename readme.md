# Dbench Application Setup and Documentation
The following documentation provides instructions on setting up and running the Dbench application using Docker Compose, Maven, and Spring Boot. We’ll cover the necessary commands, services, and how to access them.

## Prerequisites
Before proceeding, ensure that you have the following software installed:

1. **Docker**: To manage containers.
2. **Maven**: For building and packaging the application.
3. **Java (17)**: To execute the Spring Boot application.

# Steps to Set Up and Run Dbench
### 1. Docker Compose
First, start the required services using Docker Compose:

```
$ docker compose up -d
```

This command will create and start the following services:

- **PostgreSQL Database (database)**: A PostgreSQL container with the specified configuration, initialized with the script `dataset/init.sql`.
- **pgAdmin (pgadmin)**: A pgAdmin container for managing PostgreSQL.
- **Portainer (portainer)**: A Portainer container for managing Docker containers.

### 2. Maven Build
Next, build the Dbench application using Maven:

```
$ mvn clean package spring-boot:repackage
```

This command compiles the code, runs tests, and packages the application into an executable JAR file (`target/dbench-1.0-SNAPSHOT.jar`).

### 3. Run Dbench
Finally, start the Dbench application using the JAR file:

```
$ java -jar target/dbench-1.0-SNAPSHOT.jar
```

The Spring Boot application will start, and will run the benchmarks using the database and configuration provided.

## Services Overview

### 1. Database (PostgreSQL):
- Container name: `postgres`
- Image: `postgres:16.2`
- Configuration: Database name (`dbench`), username (`root`), and password (`root`)
- Ports: Exposes port `5432` for PostgreSQL
- Data volume: Mounts `pg-data` volume for persistent data

### 2. pgAdmin:
- Container name: `pgadmin`
- Image: `dpage/pgadmin4`
- Configuration: Default email (`admin@dbench.com`) and password (`root`)
- Ports: Exposes port `8888` for pgAdmin web interface
- Data volume: Mounts `pgadmin-data` volume for persistent data

### 3. Portainer:
- Container name: `portainer`
- Image: `portainer/portainer-ce:latest`
- Ports: Exposes port `9443` for Portainer web interface
- Data volume: Mounts `portainer-data` volume for persistent data

## Accessing Services
- **Dbench Application**: Access the application at http://localhost:8080.
- **pgAdmin**: Access the pgAdmin web interface at http://localhost:8888 (login with provided credentials).
- **Portainer**: Access the Portainer web interface at https://localhost:9443 (set up Portainer credentials during initial setup).

# Configuration File for Database Benchmarking (dbench)
`application-benchmark.yml` configuration file is used for configuring database benchmarking parameters and result output settings. It defines various sections related to database queries, execution settings, and result output.

It can be passed as an external parameter:

```
java -jar target/dbench-1.0-SNAPSHOT.jar --spring.config.location=[path_to_file]/application-benchmark.yml
```

## Spring Datasource Configuration

```
spring:
  datasource:
    url: jdbc:postgresql://localhost:5432/dbench
    username: root
    password: root
```

- `url`: Specifies the JDBC URL for connecting to the PostgreSQL database.
- `username` and `password`: Credentials for database authentication.

## Benchmarking Settings

### Select Query Benchmark

```
bench:
  select:
    enabled: true
    query: select * from users where id = [INT_1];
    numOfExecutions: 99
    queriesBeforeTest: 1
```

- `enabled`: Indicates whether the select query benchmark is enabled.
- `query`: The SQL select query to be executed.
- `numOfExecutions`: Number of times the query should be executed.
- `queriesBeforeTest`: Number of warm-up queries executed before the actual benchmark.

### Insert Query Benchmark

```
  insert:
    enabled: true
    query: INSERT INTO public.users(username, password, email, created_at) VALUES ([RANDOM_STRING_15], ['password'], [RANDOM_STRING_20], [RANDOM_DATE]);
    numOfExecutions: 10
    queriesBeforeTest: 1
    commitAfter: 100
```

- `enabled`: Indicates whether the insert query benchmark is enabled.
- `query`: The SQL insert query with placeholders for random strings and dates.
- `numOfExecutions`: Number of times the query should be executed.
- `queriesBeforeTest`: Number of warm-up queries executed before the actual benchmark.
- `commitAfter`: Commit after every specified number of insertions.

### Batch Insert Query Benchmark

```
  batchinsert:
    enabled: true
    query: INSERT INTO public.users(username, password, email, created_at) VALUES ([RANDOM_STRING_15], ['password'], [RANDOM_STRING_20], [RANDOM_DATE]);
    numOfExecutions: 10000000
    queriesBeforeTest: 1
    batchSize: 2
```

- `enabled`: Indicates whether the batch insert query benchmark is enabled.
- `query`: The SQL batch insert query with placeholders.
- `numOfExecutions`: Total number of batch insertions.
- `queriesBeforeTest`: Number of warm-up queries executed before the actual benchmark.
- `batchSize`: Number of records per batch.

### Placeholders
Several types of placeholders can be used as query field and will be replaced at runtime according to their type.
Those currently available are:

- `[RANDOM_STRING_X]`, where X represents the desired string length, it generates a random string of that length.
- `[RANDOM_DATE]`, it generates a random date (current timestamp).
- `[INT_X]`, where X is an integer value, it generates that fixed integer value.
- `['some_value']`, it uses the specified fixed string value.

The final query is constructed by replacing all placeholders with the `?` character (used for parameter binding in prepared statements).

## Result Output Configuration

### Console Output

```
result:
  writer:
    console:
      enabled: true
      writeExecutions: false
      timeFactor: 1000000
      unit: ms
```

- `enabled`: Enables console output for benchmark results.
- `writeExecutions`: Determines whether to display individual query execution details.
- `timeFactor`: Conversion factor for time units (e.g., microseconds to milliseconds).
- `unit`: Time unit for displaying execution times (e.g., ms for milliseconds).

### File Output

```
    file:
      enabled: true
      writeExecutions: true
      timeFactor: 1000000
      unit: ms
      folder: results
      dateFormat: yyyyMMddHHmmss
```

- `enabled`: Enables file output for benchmark results.
- `writeExecutions`: Determines whether to include individual query execution details in the output file.
- `timeFactor`: Conversion factor for time units (e.g., microseconds to milliseconds).
- `unit`: Time unit for displaying execution times (e.g., ms for milliseconds).
- `folder`: Folder where result files will be saved.
- `dateFormat`: Date format for naming result files.

Feel free to adjust the settings according to your specific use case. Happy benchmarking! 🚀
 
 
 